const express = require('express')
const router = express.Router()

const taskController = require('../controllers/taskController')

router.get('/', (req, res)=> {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})

router.get('/:id', (req,res) => {
	taskController.getTask(req.params.id).then(resultFromController => {
		if(!resultFromController){
			res.status(400).send("Something went wrong!")
		} else {
			res.json(resultFromController)
		}
	})
})

router.post('/', (req, res)=> {
	taskController.createTask(req.body).then(resultFromController => {
		if(resultFromController === "ERROR") {
			res.status(400).send("Task already inside to do list!")
		} else {
			res.send("Task saved!")
		}
	})
})


router.delete('/:id', (req, res)=> {
	taskController.deleteTask(req.params.id).then(resultFromController => {
		if(!resultFromController) {
			res.status(400).send("Something went wrong!")
		} else {
			res.send(`Task ${resultFromController.name} is deleted!`)
		}
	})
})

router.put('/:id', (req,res)=> {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => {
		if(!resultFromController) {
			res.status(400).send("Something went wrong!")
		} else {
			res.send(`Task ${req.params.id} is updated!`)
		}
	})
})

router.put('/:id/complete', (req,res) => {
	taskController.updateTaskStatus(req.params.id).then(resultFromController => {
		if(!resultFromController) {
			res.status(400).send("Something went wrong!")
		} else {
			res.send(`Task ${req.params.id}'s status is updated!`)
		}
	})
})

module.exports = router