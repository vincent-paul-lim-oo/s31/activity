const Task = require('../models/task')


module.exports.getAllTasks = ()=> {

	 return Task.find({}).then(result => {
	 	return result
	 })

}

module.exports.createTask = (requestBody) => {
	return Task.findOne({name : requestBody.name}).then(result=> {
		if(result != null && result.name == requestBody.name) {
			return 'ERROR'
		} else {
			let newTask = new Task({
				name : requestBody.name
			})

			return newTask.save().then((task, error) => {
				if(error) {
					console.log(error)
				} else {
					return task
				}
			})
		}
	})
}

module.exports.deleteTask = (id) => {
	return Task.findByIdAndRemove(id).then((removedTask, err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			return removedTask
		}
	})
}


module.exports.updateTask = (id, body) => {
	return Task.findByIdAndUpdate(id, body).then((updatedTask, err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			return updatedTask
		}
	})
}

module.exports.getTask = (id) => {
	return Task.findById({_id : id}).then((task, err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			return task
		}
	})
}

module.exports.updateTaskStatus = (id) => {
	return Task.findByIdAndUpdate(id, {status : "complete"}).then((updatedTask, err) => {
		if(err) {
			console.log(err)
			return false
		} else {
			return updatedTask
		}
	})
}